# Activity: Log Levels
For this activity, you'll be going through the code base and ensuring that the
correct log levels are used when log messages are being sent. For each log
statement you find:

- Note the current log level being used
- If it seems adequate, leave it alone
- If it seems incorrect, correct it

If you're unsure about which log level should be used, refer back to the
definitions in the lesson. 

*Note: While some of the included log messages lack context, you **don't** have to
fix these yet. We'll address this in another lesson!*

## Hints
To minimize the amount of second guessing you have to do, we have noted below
how many times each log level should be used in each module:

- cli.py
    - critical x 2
    - error x 2
    - warning x 2
    - info x 2
    - debug x 2
- db.py
    - critical x 2
    - error x 2
    - warning x 2
    - info x 2
    - debug x 2

We've also written unit tests to assert that the correct log levels were used.
You can run the whole test suite with:
```shell
$ ./poetry run test
```
