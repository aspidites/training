# LOG Levels
In this directory, you'll find a [lesson](./lesson.md), as well as the
accompanying [activity](./activity.md).

If you wandered here by accident, be sure to view the full lesson on Lessonly
first.
