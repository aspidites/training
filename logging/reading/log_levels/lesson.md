---
title: Log Levels
---

## Overview
There are five levels at which we can log information:

<dl>
    <dt>Critical</dt>
    <dd>
        Any error that is forcing a shutdown of the service or application to
        prevent data loss (or further data loss). Reserve these only for the
        most heinous errors and situations where there is guaranteed to have
        been data corruption or loss.
    </dd>
    <dt>Error</dt>
    <dd>
        Any error which is fatal to the **operation**, but not the service or
        application
    </dd>
    <dt>Warning</dt>
    <dd>
        Issue that needs to be tracked but may not require immediate intervention
    </dd>
    <dt>Info</dt>
    <dd>
        Things we want to see at high volume in case we need to forensically
        analyze an issue. System lifecycle events (system start, stop) go here.
        "Session" lifecycle events (login, logout, etc.) go here.
    </dd>
    <dt>Debug</dt>
    <dd>
        Any message that is helpful in tracking the flow through the system and
        isolating issues, especially during the development and QA phases /
        Information that is diagnostically helpful to people more than just
        developers (IT, sysadmins)
    </dd>
</dl>

As you can see from the definitions above, by simply changing the log level of
an application, we can fine tune volume and fidelity of the information we see
about a system.

## Activity
If you haven't already, clone the 
[training repo](https://gitlab.com/aspidites/training) and read the instructions
[here](https://gitlab.com/aspidites/training/blob/master/logging/log_levels/activity.md).

Once done, proceed and take the quiz here on Lessonly.
