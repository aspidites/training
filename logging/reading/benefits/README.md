# Logging Benefits
In this directory, you'll find a [lesson](./lesson.md). There is no accompanying
activity for this lesson.

If you wandered here by accident, be sure to view the full lesson on Lessonly
first.
