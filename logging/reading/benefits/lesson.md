---
title: Logging Benefits
---

## Overview
It's important for applications to produce high-quality, actionable logs, as
doing so:

- Ensures that we can quickly diagnose and address failures observed in the
  system
- Increases the confidence we have about the maintainability and stability of
  the system as a whole
