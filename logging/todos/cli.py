import logging

import click
from .db import DB

logger = logging.getLogger(__name__)


@click.group()
@click.option('-f', '--filename', default='tasks.db')
@click.option('-l', '--log-level', default="debug")
@click.pass_context
def todos(ctx, filename, log_level):
    db = DB(filename, log_level=getattr(logging, log_level.upper()))
    ctx.obj = db


@todos.command()
@click.argument('item_type', default='tasks')
@click.option('-n', '--name', multiple=True)
@click.option('-t', '--task_id')
@click.pass_context
def show(ctx, item_type, name, task_id):
    db = ctx.obj

    if item_type == 'tags':
        tags = db.show_tags(task_id)
        click.secho("Tags:", underline=True, bold=True)
        click.echo("\n".join([f"{tag.id}. {tag.name}" for tag in tags]))
    else:
        all_tasks = db.show_tasks(name)

        if not all_tasks:
            click.echo('No tasks. Try the `just start` subcommand.')
        else:
            active = [task for task in all_tasks if not task.complete]
            completed = [task for task in all_tasks if task.complete]

            click.echo('Active')
            if active:
                click.secho(
                    "\n".join([
                        f"{task.id}. {task.description} "
                        f"({', '.join([tag.name for tag in task.tags])})"
                        for task in active
                    ]),
                    bold=True)
            else:
                click.echo("You've completed all your tasks!")

            click.echo('\nCompleted')
            if completed:
                click.secho(
                    "\n".join([
                        f"{task.id}. {task.description} "
                        f"({', '.join([tag.name for tag in task.tags])})"
                        for task in completed
                    ]),
                    dim=True)
            else:
                click.echo("Looks like you've got work to do!")


@todos.command()
@click.argument('task')
@click.pass_context
def add(ctx, task):
    db = ctx.db
    db.add_task(task)
    ctx.invoke(show)


@todos.command()
@click.argument('task_id')
@click.pass_context
def remove(ctx, task_id):
    db = ctx.obj
    db.remove_task(int(task_id))
    ctx.invoke(show)


@todos.command()
@click.argument('task_id', nargs=-1)
@click.pass_context
def toggle(ctx, task_id):
    db = ctx.obj

    for task in task_id:
        db.toggle_task(int(task))

    ctx.invoke(show)


@todos.command()
@click.argument('task_id')
@click.argument('tag_name', nargs=-1)
@click.pass_context
def tag(ctx, task_id, tag_name):
    db = ctx.obj

    for tag in tag_name:
        db.tag_task(task_id, tag)

    ctx.invoke(show)


@todos.command()
@click.argument('task_id')
@click.argument('description')
@click.pass_context
def edit(ctx, task_id, description):
    db = ctx.obj
    db.edit_task(task_id, description)
    ctx.invoke(show)


if __name__ == '__main__':
    todos()
