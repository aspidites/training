import json
from logging.config import dictConfig


class PrettyEncoder(json.JSONEncoder):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indent = 4


LOGSTASH_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'logstash': {
            '()': 'logstash_formatter.LogstashFormatterV1',
            'json_cls': PrettyEncoder
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'logstash'
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        }
    }
}

config = dictConfig(LOGSTASH_CONFIG)
