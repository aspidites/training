"""
Various helper functions for performing database operations. Also includes
`Task` and `Tag` dataclasses for easily marshaling from query results.

"""
from __future__ import annotations

from dataclasses import dataclass, field
import logging
import sqlite3

logger = logging.getLogger(__name__)
conn = None


@dataclass
class Task:
    task_id: int
    description: str
    is_complete: bool = False
    tags: [Tag] = field(default_factory=list)

    @property
    def id(self):
        return self.task_id

    @property
    def complete(self):
        return bool(self.is_complete)


@dataclass
class Tag:
    tag_id: int
    name: str

    @property
    def id(self):
        return self.tag_id


class DB:
    def __init__(self, filename='tasks.db', log_level=logging.DEBUG):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(log_level)

        try:
            db_uri = f"file:{filename}?mode=rw"
            self.conn = sqlite3.connect(db_uri, uri=True)
            logger.debug(
                "Opened database", extra={"context": {
                    "db_file": filename
                }})
        except sqlite3.OperationalError:
            logger.debug(
                "Could not create database",
                exc_info=True,
                extra={"context": {
                    "db_file": filename
                }})
            exit(1)
        else:
            logger.debug(
                "Creating initial schema",
                exc_info=True,
                extra={"context": {
                    "tables": ["tasks", "tags", "task_tags"]
                }})

            with self.conn as c:
                try:
                    sql = """
                    CREATE TABLE IF NOT EXISTS tasks (
                        task_id INTEGER PRIMARY KEY,
                        description VARCHAR(255),
                        is_complete INTEGER DEFAULT 0
                    )
                    """
                    c.execute(sql)

                    sql = """
                    CREATE TABLE IF NOT EXISTS tags (
                        tag_id INTEGER PRIMARY KEY,
                        name VARCHAR(50) UNIQUE
                    )
                    """
                    c.execute(sql)

                    sql = """
                    CREATE TABLE IF NOT EXISTS task_tags (
                        task_id INTEGER,
                        tag_id INTEGER,
                        FOREIGN KEY(task_id) REFERENCES tasks(task_id)
                        FOREIGN KEY(tag_id) REFERENCES tags(tag_id)
                    )
                    """
                    c.execute(sql)
                except sqlite3.OperationalError:
                    logger.debug(
                        "Failed to create table",
                        exc_info=True,
                        extra={
                            "context": {
                                "sql_statement": " ".join(sql.strip().split())
                            }
                        })

    def show_tags(self, task_id=None):
        with self.conn as c:
            tags = []

            if task_id:
                sql = """
                SELECT tags.tag_id, name from tags
                JOIN task_tags ON tags.tag_id=task_tags.tag_id
                WHERE task_id=?
                """
            else:
                sql = "SELECT * FROM tags"

            try:
                tags = c.execute(sql, (task_id, )).fetchall()
            except sqlite3.OperationalError:
                logger.debug(
                    "Failed to show tags for task_id",
                    exc_info=True,
                    extra={
                        "context": {
                            "task_id": task_id,
                            "sql_statement": " ".join(sql.strip().split())
                        }
                    })
            finally:
                return [Tag(*tag) for tag in tags]

    def show_tasks(self, tags=None):
        tags = tags or []
        results = []
        with self.conn as c:
            sql = "SELECT * FROM tasks"
            try:
                results = c.execute(sql).fetchall()
            except sqlite3.OperationalError:
                logger.debug(
                    "Failed to show tasks",
                    exc_info=True,
                    extra={
                        "context": {
                            "tags": tags,
                            "sql_statement": " ".join(sql.strip().split())
                        }
                    })

        tasks = [Task(*task, self.show_tags(task[0])) for task in results]

        # TODO: Use proper joins instead of manual filters
        tagged_tasks = []
        if tags:
            for task in tasks:
                task_tags = {tag.name for tag in task.tags}
                if task_tags.intersection(tags):
                    tagged_tasks.append(task)
        else:
            tagged_tasks = tasks

        return tagged_tasks

    def add_task(self, description):
        with self.conn as c:
            sql = "INSERT INTO tasks (description) VALUES (?)"
            try:
                c.execute(sql, (description, ))
            except sqlite3.OperationalError:
                logger.debug(
                    "Failed to add task",
                    exc_info=True,
                    extra={
                        "context": {
                            "description": description,
                            "sql_statement": sql
                        }
                    })

    def remove_task(self, task_id):
        with self.conn as c:
            sql = "DELETE FROM tasks WHERE task_id=?"
            try:
                c.execute(sql, (task_id, ))
            except sqlite3.OperationalError:
                logger.debug(
                    "Failed to remove task",
                    exc_info=True,
                    extra={
                        "context": {
                            "task_id": task_id,
                            "sql_statement": sql
                        }
                    })

    def toggle_task(self, task_id):
        with self.conn as c:
            sql = """
            UPDATE TASKS
            SET is_complete=not is_complete WHERE task_id=?
            """
            try:
                c.execute(sql, (task_id, ))
            except sqlite3.OperationalError:
                logger.debug(
                    "Failed to toggle task",
                    exc_info=True,
                    extra={
                        "context": {
                            "task_id": task_id,
                            "sql_statement": sql
                        }
                    })

    def tag_task(self, task_id, tag_name):
        with self.conn as c:
            c.execute("""INSERT OR IGNORE INTO tags (name) VALUES (?)""",
                      (tag_name, ))

            tag = Tag(*c.execute("SELECT * FROM tags WHERE name=?",
                                 (tag_name, )).fetchone())

            c.execute("INSERT INTO task_tags (task_id, tag_id) VALUES (?, ?)",
                      (task_id, tag.tag_id))

    def edit_task(self, task_id, description):
        with self.conn as c:
            c.execute("UPDATE tasks SET description=? WHERE task_id=?",
                      (description, task_id))
