from pathlib import Path
import pytest
from todos.db import DB


@pytest.fixture(scope="module")
def db():

    filename = 'test_tasks.db'
    Path(filename).touch()

    db = DB('test_tasks.db')

    todos = [f"Test {i + 1}" for i in range(10)]
    for todo in todos:
        db.add_task(todo)

    for i in range(10):
        if i % 2 == 0:
            db.tag_task(i + 1, 'test')
        else:
            db.tag_task(i + 1, 'demo')

    yield db

    Path(filename).unlink()
