def test_schema_created(db):
    tasks = db.conn.execute("""
    SELECT name FROM sqlite_master
    WHERE type='table' AND name in ('tasks', 'tags', 'task_tags')
    """).fetchall()

    num_tasks = len(tasks)
    assert num_tasks == 3


def test_show_tags(db):
    tags = db.show_tags()

    num_tags = len(tags)
    assert num_tags == 2


def test_show_all_tasks(db):
    tasks = db.show_tasks()

    num_tasks = len(tasks)
    assert num_tasks == 10


def test_show_tagged_tasks(db):
    tasks = db.show_tasks(tags=['test'])

    num_tasks = len(tasks)
    assert num_tasks == 5


def test_add_task(db):
    db.add_task("This is a test")
    tasks = db.show_tasks()

    num_tasks = len(tasks)
    assert num_tasks == 11


def test_toggle_task(db):
    tasks = db.show_tasks()
    active = [task for task in tasks if not task.complete]

    num_active = len(active)
    assert num_active == 11

    db.toggle_task(1)
    tasks = db.show_tasks()
    active = [task for task in tasks if not task.complete]
    num_active = len(active)

    assert num_active == 10


def test_tag_task(db):
    db.tag_task(1, 'other')
    db.tag_task(1, 'else')
    tasks = db.show_tasks(['other', 'else'])

    num_tagged = len(tasks)
    assert num_tagged == 1
    num_tags = len(tasks[0].tags)
    assert num_tags == 3


def test_edit_task(db):

    db.edit_task(1, 'new description')
    task = db.show_tasks()[0]

    assert task.description == 'new description'
