# todos: A Todo Appication and Logging Tutorial
"just" is a simple todo application that allows you to manage todo tasks. In
addition to being used to help demonstrate various aspects of properly logging
activity from an application, it can be used to track the progress of various
exercises included with the lessons.

You can see an overview of commands by running `./just show` (don't forget the
leading `./`). Todos are tagged as well, so you can specify a particular tag to
show only those todos with the associated tags using `./just show -t <tag>`. For
example, `./just show -t log_levels` will show you all tasks related to log levels.

## Getting Started
If you _didn't_ arrive here from
[Lessonly](https://activecampaign.lessonly.com/learn), head there now! Once
you're ready, you can read the corresponding activity section below.

## Activities
Here, you'll find activities which correspond to the various lessons on
Lessonly. Remember that you can also filter related tasks by using a matching
tag.

## Running
You can run the todos app by executing `poetry run toods <sub-command> <options>
<arguments>`. For example

```shell
$ poetry run todos show tags
```

Will show you all tags in the database. 

We've also created a convenient alias that you can access by sourcing the
profile script:

```shell
$ source profile.sh
```

Now you can call `just` directly to use the todos app:

```shell
$ just show tags
```

### Log Levels (`./just show log_levels`)
For this set of activities, you'll be identifying points in the todo
application's source code log levels are used incorrectly and fixing them.

#### 1. Identify when 'critical' is appropriate
