# Training

This repository holds a handleful of interactive tutorials. Each tutorial
consists of:
- several smaller lessons revolving around a real Python application
- activities that aim to reinforce what you learned in those lessons1

## Using These Tutorials
While you technically *can* just clone this repository and do the tutorials
offline, we recommend heading over to [Lessonly](https://activecampaign.lessonly.com/content)
and starting the corresponding lessons. You'll then be directed here to complete
the interactive portion.

The key benefit to doing so is that your progress is tracked more carefully on
Lessonly, and it'll be easier to discover related content. The main reason for
duplicating the content in the repository is to allow you to make progress
without an internet connection.

## Available Tutorials
Here is the list of currently available tutorials and their associated lessons:

- [Logging](./logging)
    - [Log Levels](./logging/log_levels)
